<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>DomModel</name>
    <message>
        <location filename="dommodel.cpp" line="121"/>
        <source>Key</source>
        <translation>Ключ</translation>
    </message>
    <message>
        <location filename="dommodel.cpp" line="123"/>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <location filename="dommodel.cpp" line="125"/>
        <source>Value</source>
        <translation>Значение</translation>
    </message>
</context>
<context>
    <name>EditCommand</name>
    <message>
        <source>key</source>
        <translation type="obsolete">ключа</translation>
    </message>
    <message>
        <source>type</source>
        <translation type="obsolete">типа</translation>
    </message>
    <message>
        <source>value</source>
        <translation type="obsolete">значения</translation>
    </message>
</context>
<context>
    <name>EditorTab</name>
    <message>
        <location filename="editortab.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="editortab.cpp" line="163"/>
        <source>Expand</source>
        <translation>Развернуть</translation>
    </message>
    <message>
        <location filename="editortab.cpp" line="167"/>
        <source>Collapse</source>
        <translation>Свернуть</translation>
    </message>
    <message>
        <location filename="editortab.cpp" line="173"/>
        <source>Copy</source>
        <translation>Копировать</translation>
    </message>
    <message>
        <location filename="editortab.cpp" line="178"/>
        <source>Cut</source>
        <translation>Вырезать</translation>
    </message>
    <message>
        <location filename="editortab.cpp" line="185"/>
        <source>Paste</source>
        <translation>Вставить</translation>
    </message>
    <message>
        <location filename="editortab.cpp" line="190"/>
        <source>Paste as child</source>
        <translation>Вставить как подпункт</translation>
    </message>
    <message>
        <location filename="editortab.cpp" line="196"/>
        <source>New Sibling</source>
        <translation>Новая группа</translation>
    </message>
    <message>
        <location filename="editortab.cpp" line="200"/>
        <source>New Child</source>
        <translation>Новый элемент</translation>
    </message>
    <message>
        <location filename="editortab.cpp" line="407"/>
        <source>Drag and drop one or more aml files to the window to add this file.</source>
        <translation>Перетащите один или несколько файлов AML в окно, чтобы добавить этот файл.</translation>
    </message>
    <message>
        <location filename="editortab.cpp" line="412"/>
        <source>Drag and drop one or more kext files to the window to add this file.</source>
        <translation>Перетащите один или несколько файлов kext в окно, чтобы добавить этот файл.</translation>
    </message>
    <message>
        <location filename="editortab.cpp" line="417"/>
        <location filename="editortab.cpp" line="422"/>
        <source>Drag and drop one or more efi files to the window to add this file.</source>
        <translation>Перетащите один или несколько файлов efi в окно, чтобы добавить этот файл.</translation>
    </message>
</context>
<context>
    <name>EditorTabsWidget</name>
    <message>
        <location filename="editortabswidget.cpp" line="67"/>
        <source>NewFile %1</source>
        <translation>Новый файл %1</translation>
    </message>
    <message>
        <location filename="editortabswidget.cpp" line="118"/>
        <location filename="editortabswidget.cpp" line="131"/>
        <source>Close tab</source>
        <translation>Закрыть вкладку</translation>
    </message>
    <message>
        <location filename="editortabswidget.cpp" line="119"/>
        <location filename="editortabswidget.cpp" line="132"/>
        <source>Close tabs to the right</source>
        <translation>Закрыть вкладки справа</translation>
    </message>
    <message>
        <location filename="editortabswidget.cpp" line="120"/>
        <location filename="editortabswidget.cpp" line="133"/>
        <source>Close other tabs</source>
        <translation>Закрыть другие вкладки</translation>
    </message>
    <message>
        <location filename="editortabswidget.cpp" line="121"/>
        <location filename="editortabswidget.cpp" line="134"/>
        <source>Close all tabs</source>
        <translation>Закрыть все вкладки</translation>
    </message>
</context>
<context>
    <name>FileSystemWatcher</name>
    <message>
        <location filename="filesystemwatcher.cpp" line="122"/>
        <source>The file has been modified by another program. Do you want to reload?</source>
        <translation>Этот файл был изменен другой программой, вы хотите открыть его снова?</translation>
    </message>
    <message>
        <location filename="filesystemwatcher.cpp" line="124"/>
        <source>Yes</source>
        <translation>да</translation>
    </message>
    <message>
        <location filename="filesystemwatcher.cpp" line="125"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="79"/>
        <source>Tips</source>
        <translation>Советы</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="99"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="103"/>
        <source>Recent files</source>
        <translation>Недавние файлы</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="130"/>
        <source>Edit</source>
        <translation>Правка</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="156"/>
        <source>Help</source>
        <translation>Справка</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="163"/>
        <source>Preferences</source>
        <translation>Предпочтения</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="179"/>
        <source>toolBar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="230"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="250"/>
        <location filename="mainwindow.ui" line="699"/>
        <source>Go to the previous</source>
        <oldsource>Find the previous</oldsource>
        <translation>Перейти к предыдущему</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="253"/>
        <source>&lt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="260"/>
        <location filename="mainwindow.ui" line="688"/>
        <source>Go to the next</source>
        <oldsource>Find the next</oldsource>
        <translation>Перейти к следующему</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="263"/>
        <location filename="mainwindow.ui" line="297"/>
        <source>&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="273"/>
        <location filename="mainwindow.ui" line="294"/>
        <location filename="mainwindow.ui" line="710"/>
        <location filename="mainwindow.cpp" line="365"/>
        <source>Replace</source>
        <translation>Заменить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="280"/>
        <source>All</source>
        <translation>Все</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="287"/>
        <source>Hide</source>
        <translation>Спрятать</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="321"/>
        <source>Search Results</source>
        <translation>Результаты поиска</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="346"/>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="358"/>
        <location filename="mainwindow.ui" line="630"/>
        <source>Remove</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="367"/>
        <location filename="mainwindow.ui" line="370"/>
        <source>Open</source>
        <translation>Открыть</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="373"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="384"/>
        <location filename="mainwindow.ui" line="387"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="390"/>
        <source>Ctrl+W</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="398"/>
        <location filename="mainwindow.ui" line="401"/>
        <location filename="mainwindow.cpp" line="535"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="404"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="415"/>
        <location filename="mainwindow.ui" line="418"/>
        <location filename="mainwindow.cpp" line="660"/>
        <source>Save as</source>
        <translation>Сохранить как</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="421"/>
        <source>Ctrl+Alt+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="432"/>
        <source>Quit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="435"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="447"/>
        <location filename="mainwindow.cpp" line="862"/>
        <location filename="mainwindow.cpp" line="865"/>
        <location filename="mainwindow.cpp" line="867"/>
        <source>Expand all</source>
        <translation>Развернуть</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="452"/>
        <location filename="mainwindow.ui" line="455"/>
        <source>New</source>
        <translation>Новый</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="458"/>
        <source>Ctrl+N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="469"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="474"/>
        <source>File1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="482"/>
        <source>File2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="490"/>
        <source>File3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="498"/>
        <source>File4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="506"/>
        <source>File5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="514"/>
        <source>Clear list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="519"/>
        <location filename="mainwindow.ui" line="522"/>
        <source>Close all</source>
        <translation>Закрыть все</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="525"/>
        <source>Ctrl+Alt+W</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="530"/>
        <source>File6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="538"/>
        <source>File7</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="546"/>
        <source>File8</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="554"/>
        <source>File9</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="562"/>
        <source>File10</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="591"/>
        <source>RestoreScene</source>
        <translation>Восстановить сцену</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="599"/>
        <source>DefaultNodeIcon</source>
        <translation>Значок узла по умолчанию</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="604"/>
        <source>Check Update</source>
        <translation>Проверить обновление</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="609"/>
        <source>Save and refresh the data before searching</source>
        <oldsource>Save before searching(Refresh data)</oldsource>
        <translation>Сохранить и обновить данные перед поиском</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="641"/>
        <source>Expand all when opening a file</source>
        <translation>Развернуть все при открытии файла</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="646"/>
        <source>New Window</source>
        <translation>Новое окно</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="651"/>
        <source>Copy between windows</source>
        <translation>Копия между окнами</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="656"/>
        <source>Paste between windows</source>
        <translation>Вставка между окнами</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="667"/>
        <source>Show plist text</source>
        <translation>Отображает текст plist</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="672"/>
        <source>Paste as child</source>
        <translation>Вставить как подпункт</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="680"/>
        <source>Ctrl+F</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="691"/>
        <source>Ctrl+2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="702"/>
        <source>Ctrl+1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="713"/>
        <source>Ctrl+R</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="718"/>
        <source>Replace All</source>
        <translation>Заменить все</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="721"/>
        <source>Ctrl+Shift+R</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="210"/>
        <source>Undo</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="211"/>
        <source>Redo</source>
        <translation>Повторить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="570"/>
        <source>Copy</source>
        <translation>Копировать</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="575"/>
        <source>Cut</source>
        <translation>Вырезать</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="580"/>
        <source>Paste</source>
        <translation>Вставить</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="307"/>
        <source>+</source>
        <translation></translation>
    </message>
    <message>
        <source>Count</source>
        <translation type="vanished">общий итог</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="240"/>
        <location filename="mainwindow.ui" line="243"/>
        <location filename="mainwindow.ui" line="677"/>
        <location filename="mainwindow.cpp" line="363"/>
        <source>Find</source>
        <translation>Найти</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="266"/>
        <source>Expand</source>
        <translation>Развернуть</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="266"/>
        <location filename="mainwindow.cpp" line="271"/>
        <source>Collapse</source>
        <translation>Свернуть</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="267"/>
        <source>space</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="98"/>
        <source>ctrl+-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="160"/>
        <location filename="mainwindow.cpp" line="1930"/>
        <source>Total</source>
        <translation>Всего</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="536"/>
        <location filename="mainwindow.cpp" line="1745"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="537"/>
        <source>Discard</source>
        <translation>Не сохранять</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="754"/>
        <source>Last modified: </source>
        <translation>Последнее изменение: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="758"/>
        <source>Thanks: Yaroslav Sushkov (alpex92)</source>
        <oldsource>Thanks: Yaroslav Sushkov (alpex92)&lt;br&gt;</oldsource>
        <translation>Спасибо: Ярослав Сушков (alpex92)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1709"/>
        <source>Network error!</source>
        <translation>Ошибка сети!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1744"/>
        <source>New version detected!</source>
        <translation>Обнаружена новая версия!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1744"/>
        <source>Version: </source>
        <translation>Версия:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1744"/>
        <source>Published at: </source>
        <translation>Опубликовано по адресу:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1744"/>
        <source>Release Notes: </source>
        <translation>Примечания к выпуску:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1745"/>
        <source>Download</source>
        <translation>Скачать</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1750"/>
        <source>It is currently the latest version!</source>
        <translation>На данный момент это последняя версия!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1875"/>
        <source>Application</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1876"/>
        <source>Cannot read file %1:
%2.</source>
        <translation></translation>
    </message>
    <message>
        <source>Open file</source>
        <translation type="obsolete">Открыть файл</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="452"/>
        <source>Select files to open</source>
        <translation>Выберите файлы для открытия</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="660"/>
        <source>Property list (*.plist)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="532"/>
        <source>The document has been modified.</source>
        <translation>Документ был изменен.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="614"/>
        <location filename="mainwindow.cpp" line="294"/>
        <source>New Sibling</source>
        <translation>Новая группа</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="622"/>
        <location filename="mainwindow.cpp" line="301"/>
        <source>New Child</source>
        <translation>Новый элемент</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="338"/>
        <source>A-&gt;Z Sort</source>
        <translation>A-&gt;Z Сорт</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="324"/>
        <source>Move up</source>
        <translation>Двигаться вверх</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="246"/>
        <source>ctrl+alt+n</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="255"/>
        <source>shift+ctrl+v</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="260"/>
        <source>ctrl+b</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="262"/>
        <source>ctrl+alt+b</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="299"/>
        <source>ctrl++</source>
        <oldsource>ctrl+=</oldsource>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="312"/>
        <source>-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="326"/>
        <source>ctrl+u</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="330"/>
        <source>Move down</source>
        <translation>Двигаться вниз</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="332"/>
        <source>ctrl+d</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="354"/>
        <source>Find and Replace</source>
        <translation>Найти и заменить</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="355"/>
        <source>ctrl+F</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="99"/>
        <source>alt++</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="533"/>
        <source>Do you want to save your changes?</source>
        <translation>Сохранить изменения?</translation>
    </message>
    <message>
        <source>About PlistED</source>
        <translation type="vanished">О PlistED</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="862"/>
        <location filename="mainwindow.cpp" line="870"/>
        <location filename="mainwindow.cpp" line="872"/>
        <source>Collapse all</source>
        <translation>Свернуть</translation>
    </message>
    <message>
        <location filename="mainwindow.h" line="240"/>
        <source>Case sensitive</source>
        <translation>С учетом регистра букв</translation>
    </message>
    <message>
        <location filename="mainwindow.h" line="241"/>
        <source>Clear List</source>
        <translation>Очистить список</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="commands.cpp" line="23"/>
        <location filename="commands.cpp" line="24"/>
        <location filename="commands.cpp" line="172"/>
        <location filename="commands.cpp" line="174"/>
        <location filename="commands.cpp" line="175"/>
        <source>Add new item</source>
        <translation>добавление элемента</translation>
    </message>
    <message>
        <location filename="commands.cpp" line="23"/>
        <location filename="commands.cpp" line="59"/>
        <location filename="commands.cpp" line="107"/>
        <location filename="commands.cpp" line="146"/>
        <location filename="commands.cpp" line="174"/>
        <source>Undo</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <location filename="commands.cpp" line="24"/>
        <location filename="commands.cpp" line="60"/>
        <location filename="commands.cpp" line="108"/>
        <location filename="commands.cpp" line="147"/>
        <location filename="commands.cpp" line="175"/>
        <source>Redo</source>
        <translation>Повторить</translation>
    </message>
    <message>
        <location filename="commands.cpp" line="57"/>
        <source>Remove %1</source>
        <translation>удаление %1</translation>
    </message>
    <message>
        <location filename="commands.cpp" line="59"/>
        <location filename="commands.cpp" line="60"/>
        <source>Remove</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="commands.cpp" line="94"/>
        <source>key</source>
        <translation>ключа</translation>
    </message>
    <message>
        <location filename="commands.cpp" line="98"/>
        <source>type</source>
        <translation>типа</translation>
    </message>
    <message>
        <location filename="commands.cpp" line="102"/>
        <source>value</source>
        <translation>значения</translation>
    </message>
    <message>
        <location filename="commands.cpp" line="105"/>
        <location filename="commands.cpp" line="107"/>
        <location filename="commands.cpp" line="108"/>
        <source>Edit item %1</source>
        <translation>редактирование  %1</translation>
    </message>
    <message>
        <location filename="commands.cpp" line="144"/>
        <location filename="commands.cpp" line="146"/>
        <location filename="commands.cpp" line="147"/>
        <source>Paste entry</source>
        <translation>Вставить запись</translation>
    </message>
    <message>
        <location filename="domitem.cpp" line="44"/>
        <source>NewItem</source>
        <translation>Новый предмет</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1436"/>
        <source>Currently selected: </source>
        <translation>В настоящее время выбрано:  </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1437"/>
        <source>Row: </source>
        <oldsource>      Row: </oldsource>
        <translation>Ряд:  </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1438"/>
        <source>Column: </source>
        <oldsource>      Column: </oldsource>
        <translation>Колонка:  </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1439"/>
        <source>Parent level：</source>
        <oldsource>      Parent level：</oldsource>
        <translation>Родительский уровень ：</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1440"/>
        <source>Children: </source>
        <translation>Дети:  </translation>
    </message>
</context>
</TS>

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>ComboBoxDelegate</name>
    <message>
        <source>array</source>
        <translation type="obsolete">数组</translation>
    </message>
</context>
<context>
    <name>DomModel</name>
    <message>
        <location filename="dommodel.cpp" line="121"/>
        <source>Key</source>
        <translation>键</translation>
    </message>
    <message>
        <location filename="dommodel.cpp" line="123"/>
        <source>Type</source>
        <translation>数据类型</translation>
    </message>
    <message>
        <location filename="dommodel.cpp" line="125"/>
        <source>Value</source>
        <translation>值</translation>
    </message>
</context>
<context>
    <name>EditorTab</name>
    <message>
        <location filename="editortab.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="editortab.cpp" line="163"/>
        <source>Expand</source>
        <translation>展开条目</translation>
    </message>
    <message>
        <location filename="editortab.cpp" line="167"/>
        <source>Collapse</source>
        <translation>折叠条目</translation>
    </message>
    <message>
        <location filename="editortab.cpp" line="173"/>
        <source>Copy</source>
        <translation>拷贝条目</translation>
    </message>
    <message>
        <location filename="editortab.cpp" line="178"/>
        <source>Cut</source>
        <translation>剪切条目</translation>
    </message>
    <message>
        <location filename="editortab.cpp" line="185"/>
        <source>Paste</source>
        <translation>粘贴条目</translation>
    </message>
    <message>
        <location filename="editortab.cpp" line="190"/>
        <source>Paste as child</source>
        <translation>粘贴为子项</translation>
    </message>
    <message>
        <location filename="editortab.cpp" line="196"/>
        <source>New Sibling</source>
        <translation>增加同级项</translation>
    </message>
    <message>
        <location filename="editortab.cpp" line="200"/>
        <source>New Child</source>
        <translation>增加子项</translation>
    </message>
    <message>
        <location filename="editortab.cpp" line="407"/>
        <source>Drag and drop one or more aml files to the window to add this file.</source>
        <translation>拖拽一个或多个aml文件到窗口，以增加这个文件。</translation>
    </message>
    <message>
        <location filename="editortab.cpp" line="412"/>
        <source>Drag and drop one or more kext files to the window to add this file.</source>
        <translation>拖拽一个或多个kext文件到窗口，以增加这个文件。</translation>
    </message>
    <message>
        <location filename="editortab.cpp" line="417"/>
        <location filename="editortab.cpp" line="422"/>
        <source>Drag and drop one or more efi files to the window to add this file.</source>
        <translation>拖拽一个或多个efi文件到窗口，以增加这个文件。</translation>
    </message>
</context>
<context>
    <name>EditorTabsWidget</name>
    <message>
        <location filename="editortabswidget.cpp" line="67"/>
        <source>NewFile %1</source>
        <translation>新文件%1</translation>
    </message>
    <message>
        <location filename="editortabswidget.cpp" line="118"/>
        <location filename="editortabswidget.cpp" line="131"/>
        <source>Close tab</source>
        <translation>关闭标签页</translation>
    </message>
    <message>
        <location filename="editortabswidget.cpp" line="119"/>
        <location filename="editortabswidget.cpp" line="132"/>
        <source>Close tabs to the right</source>
        <translation>关闭右边的标签页</translation>
    </message>
    <message>
        <location filename="editortabswidget.cpp" line="120"/>
        <location filename="editortabswidget.cpp" line="133"/>
        <source>Close other tabs</source>
        <translation>关闭其他标签页</translation>
    </message>
    <message>
        <location filename="editortabswidget.cpp" line="121"/>
        <location filename="editortabswidget.cpp" line="134"/>
        <source>Close all tabs</source>
        <translation>关闭所有的标签页</translation>
    </message>
</context>
<context>
    <name>FileSystemWatcher</name>
    <message>
        <location filename="filesystemwatcher.cpp" line="122"/>
        <source>The file has been modified by another program. Do you want to reload?</source>
        <translation>这个文件已被其他程序修改，是否重新装入？</translation>
    </message>
    <message>
        <location filename="filesystemwatcher.cpp" line="124"/>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="filesystemwatcher.cpp" line="125"/>
        <source>No</source>
        <translation>否</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="79"/>
        <source>Tips</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="99"/>
        <source>File</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="103"/>
        <source>Recent files</source>
        <translation>最近打开的文件</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="130"/>
        <source>Edit</source>
        <translation>编辑</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="156"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="163"/>
        <source>Preferences</source>
        <translation>偏好设置</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="179"/>
        <source>toolBar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="230"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="250"/>
        <location filename="mainwindow.ui" line="699"/>
        <source>Go to the previous</source>
        <oldsource>Find the previous</oldsource>
        <translation>跳转到上一个</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="253"/>
        <source>&lt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="260"/>
        <location filename="mainwindow.ui" line="688"/>
        <source>Go to the next</source>
        <oldsource>Find the next</oldsource>
        <translation>跳转到下一个</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="263"/>
        <location filename="mainwindow.ui" line="297"/>
        <source>&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="273"/>
        <location filename="mainwindow.ui" line="294"/>
        <location filename="mainwindow.ui" line="710"/>
        <location filename="mainwindow.cpp" line="365"/>
        <source>Replace</source>
        <translation>替换</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="280"/>
        <source>All</source>
        <translation>全部</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="287"/>
        <source>Hide</source>
        <translation>隐藏</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="321"/>
        <source>Search Results</source>
        <translation>搜索结果</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="346"/>
        <source>Add</source>
        <translation>增加条目</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="358"/>
        <location filename="mainwindow.ui" line="630"/>
        <source>Remove</source>
        <translation>移除条目</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="367"/>
        <location filename="mainwindow.ui" line="370"/>
        <source>Open</source>
        <translation>打开文件</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="373"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="384"/>
        <location filename="mainwindow.ui" line="387"/>
        <source>Close</source>
        <translation>关闭标签页</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="390"/>
        <source>Ctrl+W</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="398"/>
        <location filename="mainwindow.ui" line="401"/>
        <location filename="mainwindow.cpp" line="535"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="404"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="415"/>
        <location filename="mainwindow.ui" line="418"/>
        <location filename="mainwindow.cpp" line="660"/>
        <source>Save as</source>
        <translation>另存</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="421"/>
        <source>Ctrl+Alt+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="432"/>
        <source>Quit</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="435"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="447"/>
        <location filename="mainwindow.cpp" line="862"/>
        <location filename="mainwindow.cpp" line="865"/>
        <location filename="mainwindow.cpp" line="867"/>
        <source>Expand all</source>
        <translation>展开所有</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="452"/>
        <location filename="mainwindow.ui" line="455"/>
        <source>New</source>
        <translation>新建文件</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="458"/>
        <source>Ctrl+N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="469"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="474"/>
        <source>File1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="482"/>
        <source>File2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="490"/>
        <source>File3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="498"/>
        <source>File4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="506"/>
        <source>File5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="514"/>
        <source>Clear list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="519"/>
        <location filename="mainwindow.ui" line="522"/>
        <source>Close all</source>
        <translation>关闭所有的标签页</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="525"/>
        <source>Ctrl+Alt+W</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="530"/>
        <source>File6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="538"/>
        <source>File7</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="546"/>
        <source>File8</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="554"/>
        <source>File9</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="562"/>
        <source>File10</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="591"/>
        <source>RestoreScene</source>
        <translation>现场还原</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="599"/>
        <source>DefaultNodeIcon</source>
        <translation>默认的节点图标</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="604"/>
        <source>Check Update</source>
        <translation>检查更新</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="609"/>
        <source>Save and refresh the data before searching</source>
        <oldsource>Save before searching(Refresh data)</oldsource>
        <translation>查找之前先保存并更新数据</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="641"/>
        <source>Expand all when opening a file</source>
        <translation>打开文件时全部展开</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="646"/>
        <source>New Window</source>
        <translation>新建窗口</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="651"/>
        <source>Copy between windows</source>
        <translation>窗口之间拷贝</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="656"/>
        <source>Paste between windows</source>
        <translation>窗口之间粘贴</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="667"/>
        <source>Show plist text</source>
        <translation>显示plist文本</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="672"/>
        <source>Paste as child</source>
        <translation>粘贴为子项</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="680"/>
        <source>Ctrl+F</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="691"/>
        <source>Ctrl+2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="702"/>
        <source>Ctrl+1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="713"/>
        <source>Ctrl+R</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="718"/>
        <source>Replace All</source>
        <translation>全部替换</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="721"/>
        <source>Ctrl+Shift+R</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="210"/>
        <source>Undo</source>
        <translation>撤销</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="211"/>
        <source>Redo</source>
        <translation>恢复</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="570"/>
        <source>Copy</source>
        <translation>拷贝条目</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="575"/>
        <source>Cut</source>
        <translation>剪切条目</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="580"/>
        <source>Paste</source>
        <translation>粘贴条目</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="614"/>
        <location filename="mainwindow.cpp" line="294"/>
        <source>New Sibling</source>
        <translation>增加同级项</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="622"/>
        <location filename="mainwindow.cpp" line="301"/>
        <source>New Child</source>
        <translation>增加子项</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="338"/>
        <source>A-&gt;Z Sort</source>
        <translation>A-&gt;Z 排序</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="324"/>
        <source>Move up</source>
        <translation>条目上移</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="246"/>
        <source>ctrl+alt+n</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="255"/>
        <source>shift+ctrl+v</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="260"/>
        <source>ctrl+b</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="262"/>
        <source>ctrl+alt+b</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="299"/>
        <source>ctrl++</source>
        <oldsource>ctrl+=</oldsource>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="307"/>
        <source>+</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="312"/>
        <source>-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="326"/>
        <source>ctrl+u</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="330"/>
        <source>Move down</source>
        <translation>条目下移</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="332"/>
        <source>ctrl+d</source>
        <translation></translation>
    </message>
    <message>
        <source>Count</source>
        <translation type="vanished">计数</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="240"/>
        <location filename="mainwindow.ui" line="243"/>
        <location filename="mainwindow.ui" line="677"/>
        <location filename="mainwindow.cpp" line="363"/>
        <source>Find</source>
        <translation>查找</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="354"/>
        <source>Find and Replace</source>
        <translation>查找和替换</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="355"/>
        <source>ctrl+F</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="266"/>
        <source>Expand</source>
        <translation>展开条目</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="266"/>
        <location filename="mainwindow.cpp" line="271"/>
        <source>Collapse</source>
        <translation>折叠条目</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="267"/>
        <source>space</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="98"/>
        <source>ctrl+-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="99"/>
        <source>alt++</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="160"/>
        <location filename="mainwindow.cpp" line="1930"/>
        <source>Total</source>
        <translation>共计</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="452"/>
        <source>Select files to open</source>
        <translation>选择一个打开的文件</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="532"/>
        <source>The document has been modified.</source>
        <translation>文件内容已修改。</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="533"/>
        <source>Do you want to save your changes?</source>
        <translation>是否保存？</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="536"/>
        <location filename="mainwindow.cpp" line="1745"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="537"/>
        <source>Discard</source>
        <translation>放弃</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="660"/>
        <source>Property list (*.plist)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="754"/>
        <source>Last modified: </source>
        <translation>最后修改：</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="758"/>
        <source>Thanks: Yaroslav Sushkov (alpex92)</source>
        <oldsource>Thanks: Yaroslav Sushkov (alpex92)&lt;br&gt;</oldsource>
        <translation>致谢：Yaroslav Sushkov(alpex92)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="862"/>
        <location filename="mainwindow.cpp" line="870"/>
        <location filename="mainwindow.cpp" line="872"/>
        <source>Collapse all</source>
        <translation>折叠所有</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1709"/>
        <source>Network error!</source>
        <translation>网络错误！</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1744"/>
        <source>New version detected!</source>
        <translation>检测到新版本！</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1744"/>
        <source>Version: </source>
        <translation>版本：</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1744"/>
        <source>Published at: </source>
        <translation>更新时间：</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1744"/>
        <source>Release Notes: </source>
        <translation>更新说明：</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1745"/>
        <source>Download</source>
        <translation>下载</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1750"/>
        <source>It is currently the latest version!</source>
        <translation>目前是最新版本！</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1875"/>
        <source>Application</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1876"/>
        <source>Cannot read file %1:
%2.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.h" line="240"/>
        <source>Case sensitive</source>
        <translation>区分大小写</translation>
    </message>
    <message>
        <location filename="mainwindow.h" line="241"/>
        <source>Clear List</source>
        <translation>清除列表</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="commands.cpp" line="23"/>
        <location filename="commands.cpp" line="24"/>
        <location filename="commands.cpp" line="172"/>
        <location filename="commands.cpp" line="174"/>
        <location filename="commands.cpp" line="175"/>
        <source>Add new item</source>
        <translation>增加新条目</translation>
    </message>
    <message>
        <location filename="commands.cpp" line="23"/>
        <location filename="commands.cpp" line="59"/>
        <location filename="commands.cpp" line="107"/>
        <location filename="commands.cpp" line="146"/>
        <location filename="commands.cpp" line="174"/>
        <source>Undo</source>
        <translation>撤销</translation>
    </message>
    <message>
        <location filename="commands.cpp" line="24"/>
        <location filename="commands.cpp" line="60"/>
        <location filename="commands.cpp" line="108"/>
        <location filename="commands.cpp" line="147"/>
        <location filename="commands.cpp" line="175"/>
        <source>Redo</source>
        <translation>恢复</translation>
    </message>
    <message>
        <location filename="commands.cpp" line="57"/>
        <source>Remove %1</source>
        <translation>移除 %1</translation>
    </message>
    <message>
        <location filename="commands.cpp" line="59"/>
        <location filename="commands.cpp" line="60"/>
        <source>Remove</source>
        <translation>移除条目</translation>
    </message>
    <message>
        <location filename="commands.cpp" line="94"/>
        <source>key</source>
        <translation>键</translation>
    </message>
    <message>
        <location filename="commands.cpp" line="98"/>
        <source>type</source>
        <translation>数据类型</translation>
    </message>
    <message>
        <location filename="commands.cpp" line="102"/>
        <source>value</source>
        <translation>值</translation>
    </message>
    <message>
        <location filename="commands.cpp" line="105"/>
        <location filename="commands.cpp" line="107"/>
        <location filename="commands.cpp" line="108"/>
        <source>Edit item %1</source>
        <translation>编辑的条目 %1</translation>
    </message>
    <message>
        <location filename="commands.cpp" line="144"/>
        <location filename="commands.cpp" line="146"/>
        <location filename="commands.cpp" line="147"/>
        <source>Paste entry</source>
        <translation>粘贴条目</translation>
    </message>
    <message>
        <source>Item %1</source>
        <translation type="obsolete">条目 %1</translation>
    </message>
    <message>
        <location filename="domitem.cpp" line="44"/>
        <source>NewItem</source>
        <translation>新条目</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1436"/>
        <source>Currently selected: </source>
        <translation>当前：</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1437"/>
        <source>Row: </source>
        <oldsource>      Row: </oldsource>
        <translation>行：</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1438"/>
        <source>Column: </source>
        <oldsource>      Column: </oldsource>
        <translation>列：</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1439"/>
        <source>Parent level：</source>
        <oldsource>      Parent level：</oldsource>
        <translation>父级：</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1440"/>
        <source>Children: </source>
        <translation>子项：</translation>
    </message>
</context>
</TS>
